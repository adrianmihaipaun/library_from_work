<?php
session_start();
include 'connect.php';
include 'registerfunc.php';
?>

<?php
//verifica daca exista date trimise din formularul de register
if (!empty($_POST['send'])) {

    $connection = sqlConnect();
    $error = dataValidate($_POST);
    if (empty($error) && !empty($connection)) {
        //daca nu sunt erori cu privire la validarea userului, datele din formular sunt adaugat in baza de date, dupa care se face redirect la pagina de login
        mysqli_query($connection, "INSERT INTO `users` (name, email, password) VALUES ('".$_POST['Name']."', '".$_POST['Email']."', '".$_POST['Password']."')");

        header('Location: login.php');
        exit();
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-color: #cccccc ">
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">My website</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>

        </ul>
    </div>
</nav>

<div class="container">
    <h2>Register</h2>
    <form action="register.php" method="POST">
        <!-- daca sunt erori pe campul name se va afisa un mesaj de eroare -->
        <?php echo !empty($error['Name']) ? $error['Name'] : ""?><br/>
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="Name" class="form-control" id="Name" placeholder="Enter Name" name="Name">
        </div>
        <!-- daca sunt erori pe campul email se va afisa un mesaj de eroare -->
        <?php echo !empty($error['Email']) ? $error['Email'] : ""?><br/>
        <div class="form-group">
            <label for="Email">Email:</label>
            <input type="Email" class="form-control" id="Email" placeholder="Enter Email" name="Email">
        </div>
        <!-- daca sunt erori pe campul password se va afisa un mesaj de eroare -->
        <?php echo !empty($error['Password']) ? $error['Password'] : ""?><br/>
        <div class="form-group">
            <label for="Password">Password:</label>
            <input type="Password" class="form-control" id="Password" placeholder="Enter password" name="Password">
        </div>
        <!-- daca sunt erori pe campul confirm password se va afisa un mesaj de eroare -->
        <?php echo !empty($error['Cpassword']) ? $error['Cpassword'] : ""?><br/>
        <div class="form-group">
            <label for="Cpassword">Confirm password:</label>
            <input type="Password" class="form-control" id="Cpassword" placeholder="Enter password" name="Cpassword">
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="remember"> Remember me</label>
        </div>
        <input type="submit" name="send" class="btn btn-primary">

    </form>
</div>
</body>
</html>




