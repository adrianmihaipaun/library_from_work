<?php
session_start();
include 'connect.php';
include 'database_validate.php';
//verifica daca este setata sesiunea,  afiseaza aceasta pagina, daca nu face redirect carte index.php
if (!isset($_SESSION['user'])) {
    $_SESSION['problems'] = true;
    header("Location: index.php");


    exit();
}
//verifica daca sunt date in formularul de introducere a autorului
if (!empty($_POST['send'])) {
    $fails = validateAddAuthors($_POST);

    if (empty($fails)) {
        $conn = sqlConnect($_POST);
        $user_id = $_SESSION['user']['id'];
        //adauga datele din formular in baza de date
        mysqli_query($conn, "INSERT INTO authors (name, user_id) VALUES ('".$_POST['author_name']."', $user_id)");
    }
}

$conn = sqlConnect();
$user_id = $_SESSION['user']['id'];
//se verifica daca exista date in formulat
if (!empty($_POST['add_book']) ) {
    $fail = validateAddBooks($_POST);
    if (empty($fail)) {

        //introduce datele din formular in tabela, pe baza id-ului autorului
        mysqli_query($conn, "INSERT INTO books (name, genre, year, user_id) VALUES ('".$_POST['book_name']."', '".$_POST['book_genre']."', '".$_POST['book_year']."', $user_id)");
        $book_id = $conn->insert_id;
        foreach ($_POST['author_name'] as $key=>$author_id ){
            mysqli_query($conn, "INSERT INTO author_books (author_id, book_id) VALUES (".$author_id.", ".$book_id.")");
        }
    }
}

?>

    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title></title>
    </head>
    <body style="background-color: #cccccc ">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">My website</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Account <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="contact.php">Contact</a></li>
                        <li><a href="http://www.facebook.com">Facebook</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret" class="glyphicon glyphicon-book"></span> Your Library</a>
                    <ul class="dropdown-menu">
                        <li><a href="add_to_database.php">Add books</a></li>
                        <li><a href="show_your_books.php">View your books</a></li>
                        <li><a href="update_database.php">Update</a></li>
                        <li><a href="search_books.php">Search for books</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
            </ul>
        </div>
    </nav>

    <div class="container">
        <div>
            <?php echo $_SESSION['user']['name']. "&nbsp now you add the authors or a book on your profile!"; ?>
        </div>
        <h2>Add Author</h2>

        <form action="add_to_database.php" method="POST">
            <div class="form-group">
                <?php echo  !empty($fails['author_name']) ? $fails['author_name'] : "" ?><br/>
                <label for="author_name">Author Name:<br/></label>
                <input type="text" class="form-control" id="author_name" placeholder="Enter author_name" name="author_name">
            </div>
            <input type="submit" name="send" value="Add Author" class="btn btn-success">
        </form>
    </div>
    </body>
    </html>



    <div class="container">
        <h2>Add Books</h2>
        <form action="add_to_database.php" method="POST">

            <?php

            $conn = sqlConnect();
            //selecteaza coloanele id si name din tabela auhors
            $result = mysqli_query($conn, "SELECT id, name FROM `authors`");
            echo "<select multiple='multiple' name= 'author_name[]'>";

            while($drop=mysqli_fetch_assoc($result)){
                // preia toate valorile din coloanele de mai sus si le afiseaza in dropdown
                echo "<option value='".$drop['id']."'>".$drop['name']. "</option>";

            }
            echo "</select>";
            ;
            ?>

            <div class="form-group">
                <?php echo !empty($fail['book_name']) ? $fail['book_name'] : ""?><br/>
                <label for="book_name">Book Name:</label>
                <input type="text" class="form-control" id="book_name" placeholder="Enter book_name" name="book_name">
            </div>
            <?php echo !empty($fail['book_genre']) ? $fail['book_genre'] : ""?><br/>
            <div class="form-group">
                <label for="book_genre">Book Genre:</label>
                <input type="text" class="form-control" id="book_genre" placeholder="Enter book_genre" name="book_genre">
            </div>
            <?php echo !empty($fail['book_year']) ? $fail['book_year'] : ""?><br/>
            <div class="form-group">
                <label for="book_year">Year of Book born:</label>
                <input type="text" class="form-control" id="book_year" placeholder="Enter book_year" name="book_year">
            </div>
            <input type="submit" name="add_book" value="Add Book" class="btn btn-success">
        </form>
    </div>
<?php



