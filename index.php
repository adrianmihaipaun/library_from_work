<?php
session_start();
include 'connect.php';
include 'registerfunc.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title></title>
</head>
<body style="background-color: #cccccc ">
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">My website</a>
        </div>
        <ul class="nav navbar-nav">
            <?php if (isset($_SESSION['user']['name'])) {
                ?>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Account <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="contact.php">Contact</a></li>
                        <li><a href="http://www.facebook.com">Facebook</a></li>
                    </ul>
                </li>
                <?php
            }
            ?>

            <!-- se verifica daca este setata sesiunea si afisaza link-urile in functie de aceasta -->
            <?php if (isset($_SESSION['user']['name'])) {
            ?>
            <li><a href="products.php"><span class="glyphicon glyphicon-book"></span> Your Library</a>
                <?php
                }
                ?>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <?php if (!isset($_SESSION['user']['name'])) {
                ?>
                <li><a href="register.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <?php
            }
            ?>
            <?php if (isset($_SESSION['user']['name'])) {
                ?>
                <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
                <?php
            } else {
                ?>
                <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                <?php
            }
            ?>

        </ul>
    </div>
</nav>
<div class="container">
    <div>
        <!-- daca este setata sesiunea afisaza un mesaj insotit de numele de utilizator din tabela users -->
        <?php  if (!empty($_SESSION['user']['name'])) { echo "<h3>Welcome</h3> &nbsp" .$_SESSION['user']['name']; }?>
        <!-- cand se incearca accesarea altor pagini decat index-ul si sesiunea nu este setata, se afisaza un mesaj -->
        <?php if (!empty($_SESSION['problems'])) {echo "Trebuie sa te loghezi sau sa iti faci un cont!"; session_destroy(); }?>
    </div>
</div>

</body>
</html>