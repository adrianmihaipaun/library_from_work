<?php
session_start();
//verifica daca este setata sesiunea,  afiseaza aceasta pagina, daca nu face redirect carte index.php
if (!isset($_SESSION['user'])) {
    header("Location: index.php");
    $_SESSION['problems'] = true;

    exit();
}

?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title></title>
</head>
<body style="background-color: #cccccc ">
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">My website</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Account <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="contact.php">Contact</a></li>
                    <li><a href="http://www.facebook.com">Facebook</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret" class="glyphicon glyphicon-book"></span> Your Library</a>
                <ul class="dropdown-menu">
                    <li><a href="add_to_database.php">Add books</a></li>
                    <li><a href="show_your_books.php">View your books</a></li>
                    <li><a href="update_database.php">Update</a></li>
                    <li><a href="search_books.php">Search for books</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
        </ul>
    </div>
</nav>
<div class="container">
    <div>
        <?php echo "Welcome to your library &nbsp " .$_SESSION['user']['name']; ?>
    </div>
</div>


</body>
</html>
