<?php
function validateAddBooks($post)
{
    $fail = [];
    if (empty($post['book_name'])) {
        echo $fail['book_name'] = "Nu ati introdus numele cartii!";
    }
    if (empty($post['book_genre'])) {
        echo $fail['book_genre'] = 'Nu ati introdus genul cartii!';
    }
    if (empty($post['book_year'])) {
        echo $fail['book_year'] = 'Nu ati introdus anul aparitiei cartii!';
    }
    return $fail;
}

function validateAddAuthors($post)
{
    $fails = [];
    if (empty($post['author_name'])) {
        echo $fails['author_name'] = 'Nu ati introdus numele autorului!';
    }
    return $fails;
}
?>