<?php
include 'loginfunc.php';
include 'connect.php';

if (!empty($_POST['login'])) {
    session_start();
    $connection = sqlconnect();
    $status = loginValidate($_POST);
    //se verifica daca acele campuri din formular contin sau nu date
    if (empty($status)) {
        // se compara email-ul din formular cu cele din tabela de useri
        $user = getUserByEmail($_POST['Email']);
        var_dump($user);

        if (userIsValid($user, $_POST['Password'])) {
            //se verifica daca parola introdusa este aceeasi cu cea din tabela
            logUser($user);
            header("Location: index.php");
        } else {
            $status['Password'] = "Userul nu este valid";
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-color: #cccccc ">

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">My website</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="register.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        </ul>
    </div>
</nav>
<div class="container">
    <h2>Log In</h2>
    <form action="login.php" method="POST">
        <!-- daca nu au fost introduse date in formular sau nu sunt valide se afisaza un mesaj de eroare -->
        <?php if (!empty($status['Password']) || !empty($status['Email'])) {
            echo "Login nereusit";
        }
        ?>
        <div class="form-group">
            <label for="Email">Email:</label>
            <input type="Email" class="form-control" id="Email" placeholder="Enter Email" name="Email">
        </div>
        <div class="form-group">
            <label for="Password">Password:</label>
            <input type="Password" class="form-control" id="Password" placeholder="Enter password" name="Password">
        </div>
        <div class="form-group">
            <label for="Cpassword">Confirm password:</label>
            <input type="Password" class="form-control" id="Cpassword" placeholder="Enter Cpassword" name="Cpassword">
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="remember"> Remember me</label>
        </div>
        <input type="submit" name="login" class="btn btn-primary">
    </form>
</div>

</body>
</html>

