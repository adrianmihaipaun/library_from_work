<?php
session_start();
include 'connect.php';
$conn = sqlConnect();
//verifica daca este setata sesiunea,  afiseaza aceasta pagina, daca nu face redirect carte index.php
if (!isset($_SESSION['user'])) {
    header("Location: index.php");
    $_SESSION['problems'] = true;

    exit();
}

?>

    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title></title>
    </head>
    <body style="background-color: #cccccc ">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">My website</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Account <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="contact.php">Contact</a></li>
                        <li><a href="http://www.facebook.com">Facebook</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret" class="glyphicon glyphicon-book"></span> Your Library</a>
                    <ul class="dropdown-menu">
                        <li><a href="add_to_database.php">Add books</a></li>
                        <li><a href="show_your_books.php">View your books</a></li>
                        <li><a href="update_database.php">Update</a></li>
                        <li><a href="search_books.php">Search for books</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div>
            <?php echo $_SESSION['user']['name']. "&nbsp you now search for books!"; ?>

        </div>
        <div class="container">
            <h2>Search for books</h2>
            <form action="search_books.php" method="POST">
                <div class="form-group">
                    <label for="author_name">Search:</label>
                    <input type="text" class="form-control" id="author_name" placeholder="Enter author_name" name="book_name">
                </div>
                <input type="submit" name="search" class="btn btn-success">
            </form>
        </div>
    </div>

    </body>
    </html>

<?php


// verifica daca exista date trimise din formular
if (!empty($_POST['search'])) {
    $book_name = $_POST['book_name'];
    // cauta in tabela books valoarea trimisa prin formular
    $sql = "SELECT * From `books` WHERE `name` = '$book_name'";
    $result = $conn->query($sql);
    // afisaza valorile gasite in formular, in urma cautarii anterioare
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo '<div style="margin-left: 200px; margin-top:10px;"><br/> Name: '.$row['name'].' <br/> Genre: ' .$row['genre'].' <br/> Year: '.$row['year'].'</div>';
        }
    } else {
        echo "0 results";
    }
    $conn->close();

}
