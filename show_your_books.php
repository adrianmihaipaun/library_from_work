<?php
session_start();
include 'connect.php';
//verifica daca este setata sesiunea,  afiseaza aceasta pagina, daca nu face redirect carte index.php
if (!isset($_SESSION['user'])) {
    header("Location: index.php");
    $_SESSION['problems'] = true;

    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title></title>
</head>
<body style="background-color: #cccccc ">
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">My website</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Account <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="contact.php">Contact</a></li>
                    <li><a href="http://www.facebook.com">Facebook</a></li>
                </ul>
            </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret" class="glyphicon glyphicon-book"></span> Your Library</a>
                <ul class="dropdown-menu">
                    <li><a href="add_to_database.php">Add books</a></li>
                    <li><a href="show_your_books.php">View your books</a></li>
                    <li><a href="update_database.php">Update</a></li>
                    <li><a href="search_books.php">Search for books</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
        </ul>
    </div>
</nav>
<div class="container">
    <div>
        <?php echo $_SESSION['user']['name']. "&nbsp now you see your books!"; ?>

    </div>
    <div class="container">
        <h2>Your books</h2>
    </div>
</div>


</body>
</html>

<?php


$session_id = $_SESSION['user']['id'];
$conn = sqlConnect();
// selecteaza din tabela books toate datele ce au id-ul userului din sesiune
$sql = "SELECT * FROM `books` where `user_id` = '$session_id'";
$result = $conn->query($sql);
//verifica daca exista rezultates
if ($result->num_rows >0) {

    while ($row = $result->fetch_assoc()) {
        // afisaza din tabela toate datele ce au id-ul egal cu id-ul userului din sesiune
        echo '<div style="margin-left:200px; margin-top:15px;"><br/> Book Name:' .$row['name']. '<br/> Book Genre: '.$row['genre']. '<br/>Year of Book Publication:' .$row['year'].'</div><br/><br/>';
    }
} else {
    echo "0 rows";
}
$conn->close();
?>
